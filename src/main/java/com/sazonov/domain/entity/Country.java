package com.sazonov.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The type Country is entity class used for storage
 * information about Country.
 *
 * @author alexander
 * @version 1.0
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "country")
public class Country implements Serializable {

    private static final long serialVersionUID = 43L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;
}
