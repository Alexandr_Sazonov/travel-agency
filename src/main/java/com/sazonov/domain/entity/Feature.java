package com.sazonov.domain.entity;


/**
 * The Feature is enum used for storage
 * some features.
 *
 * @author alexander
 * @version 1.0
 */
public enum Feature {

    music, tv, wifi, phone, massage, computer, bar, bitch, cola, sprite, none
}
