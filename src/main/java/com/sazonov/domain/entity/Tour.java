package com.sazonov.domain.entity;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The type Tour is entity class used for storage
 * information about Tour.
 *
 * @author alexander
 * @version 1.0
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tour")
@TypeDef(
        name = "tour_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Tour implements Serializable {

    private static final long serialVersionUID = 45L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "photo")
    private String photo;

    @Column(name = "date")
    private Date date;

    @Column(name = "duration")
    private int duration;

    @Column(name = "description")
    private String description;

    @Column(name = "cost")
    private BigDecimal cost;

    @Enumerated(EnumType.STRING)
    @Type(type = "tour_enum")
    @Column(name = "tour_type")
    private TourType tourType;

    @Column(name = "hotel_id")
    private int hotelId;

    @Column(name = "country_id")
    private int countryId;
}
