package com.sazonov.domain.entity;

import com.vladmihalcea.hibernate.type.array.EnumArrayType;
import lombok.*;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Hotel is entity class used for storage
 * information about Hotel.
 *
 * @author alexander
 * @version 1.0
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "hotel")
@TypeDef(
        typeClass = EnumArrayType.class,
        defaultForType = Feature[].class,
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = EnumArrayType.SQL_ARRAY_TYPE,
                        value = "features"
                )
}
)
public class Hotel implements Serializable {

    private static final long serialVersionUID = 44L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "stars")
    private int stars;

    @Column(name = "website")
    private String website;

    @Column(name = "lalitude")
    private String lalitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(
            name = "features",
            columnDefinition = "features[]"
    )
    private Feature[] features;
}
