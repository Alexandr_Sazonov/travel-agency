package com.sazonov.domain.entity;

/**
 * The enum Tour type.
 */
public enum TourType {
    family,
    two,
    one,
    hot,
    bar,
    foo,
    tor,
    top,
    acc
}
