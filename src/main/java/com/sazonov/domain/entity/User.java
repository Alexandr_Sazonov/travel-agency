package com.sazonov.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The type User is entity class used for storage
 * information about User.
 * 
 * @author alexander
 * @version 1.0
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;
}
