package com.sazonov.repository.jpa.impl;

import com.sazonov.domain.entity.Country;
import com.sazonov.repository.CrudRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import java.util.List;

@Repository
public class CountryJpaRepository implements CrudRepository<Country> {

    @Autowired
    SessionFactory sessionFactory;

    CriteriaBuilder builder;
    CriteriaQuery<Country> criteria;
    Root<Country> root;
    Query<Country> query;

    @Override
    public List<Country> getAll() {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Country.class);
        root = criteria.from(Country.class);

        criteria.select(root);

        query = session.createQuery(criteria);

        return query.getResultList();
    }

    @Override
    public Country getEntityById(int id) {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Country.class);
        root = criteria.from(Country.class);

        criteria.select(root);
        query = session.createQuery(criteria);

        Country country = query.setMaxResults(1).uniqueResult();
        session.close();

        return country;
    }

    @Override
    @Transactional
    public boolean removeById(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Country country = new Country();
        country.setId(id);

        session.delete(country);

        transaction.commit();
        session.close();

        return false;
    }

    @Override
    public boolean add(Country country) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(country);
        transaction.commit();
        session.close();

        return false;
    }


    @Override
    @Transactional
    public boolean update(Country country) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();


        builder = session.getCriteriaBuilder();
        CriteriaUpdate<Country> criteriaUpdate = builder.createCriteriaUpdate(Country.class);
        root = criteriaUpdate.from(Country.class);

        criteriaUpdate.set("name", country.getName());
        criteriaUpdate.where(builder.equal(root.get("id"), country.getId()));

        int actualResult = session.createQuery(criteriaUpdate).executeUpdate();

        transaction.commit();
        session.close();

        return actualResult == 1;
    }
}
