package com.sazonov.repository.jpa.impl;

import com.sazonov.domain.entity.Tour;
import com.sazonov.repository.CrudRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class TourJpaRepository implements CrudRepository<Tour> {

    @Autowired
    SessionFactory sessionFactory;

    CriteriaBuilder builder;
    CriteriaQuery<Tour> criteria;
    Root<Tour> root;
    Query<Tour> query;

    @Override
    public List<Tour> getAll() {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Tour.class);
        root = criteria.from(Tour.class);

        criteria.select(root);
        query = session.createQuery(criteria);

        return query.getResultList();
    }

    @Override
    public Tour getEntityById(int id) {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Tour.class);
        root = criteria.from(Tour.class);

        criteria.select(root).where(builder.equal(root.get("id"), id));

        return session.createQuery(criteria).getSingleResult();
    }

    @Override
    @Transactional
    public boolean removeById(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Tour tour = new Tour();
        tour.setId(id);

        session.delete(tour);
        transaction.commit();
        session.close();

        return false;
    }

    @Override
    @Transactional
    public boolean add(Tour tour) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(tour);
        transaction.commit();
        session.close();

        return false;
    }

    @Override
    @Transactional
    public boolean update(Tour tour) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        builder = session.getCriteriaBuilder();
        CriteriaUpdate<Tour> criteriaUpdate = builder.createCriteriaUpdate(Tour.class);
        root = criteriaUpdate.from(Tour.class);

        criteriaUpdate.set("photo", tour.getPhoto());
        criteriaUpdate.set("date", tour.getDate());
        criteriaUpdate.set("duration", tour.getDuration());
        criteriaUpdate.set("description", tour.getDescription());
        criteriaUpdate.set("cost", tour.getCost());

        criteriaUpdate.where(builder.equal(root.get("id"), tour.getId()));

        int actualResult = session.createQuery(criteriaUpdate).executeUpdate();

        transaction.commit();
        session.close();

        return actualResult == 1;
    }
}
