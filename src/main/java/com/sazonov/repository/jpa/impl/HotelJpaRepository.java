package com.sazonov.repository.jpa.impl;

import com.sazonov.domain.entity.Hotel;
import com.sazonov.repository.CrudRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class HotelJpaRepository implements CrudRepository<Hotel> {

    @Autowired
    SessionFactory sessionFactory;

    CriteriaBuilder builder;
    CriteriaQuery<Hotel> criteria;
    Root<Hotel> root;
    Query<Hotel> query;

    @Override
    public List<Hotel> getAll() {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Hotel.class);
        root = criteria.from(Hotel.class);

        criteria.select(root);

        return session.createQuery(criteria).getResultList();
    }

    @Override
    public Hotel getEntityById(int id) {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Hotel.class);
        root = criteria.from(Hotel.class);

        criteria.select(root).where(builder.equal(root.get("id"), id));


        return session.createQuery(criteria).getSingleResult();
    }

    @Override
    @Transactional
    public boolean removeById(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Hotel hotel = new Hotel();
        hotel.setId(id);

        session.delete(hotel);

        transaction.commit();
        session.close();

        return false;
    }

    @Override
    @Transactional
    public boolean add(Hotel hotel) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(hotel);

        transaction.commit();
        session.close();

        return false;
    }

    @Override
    @Transactional
    public boolean update(Hotel hotel) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        builder = session.getCriteriaBuilder();
        CriteriaUpdate<Hotel> criteriaUpdate = builder.createCriteriaUpdate(Hotel.class);
        root = criteriaUpdate.from(Hotel.class);

        criteriaUpdate.set("name", hotel.getName());
        criteriaUpdate.set("stars", hotel.getStars());
        criteriaUpdate.set("website", hotel.getWebsite());
        criteriaUpdate.set("lalitude", hotel.getLalitude());
        criteriaUpdate.set("longitude", hotel.getLongitude());
        criteriaUpdate.set("features", hotel.getFeatures());

        criteriaUpdate.where(builder.equal(root.get("id"), hotel.getId()));

        int actualResult = session.createQuery(criteriaUpdate).executeUpdate();

        transaction.commit();
        session.close();

        return actualResult == 1;
    }
}
