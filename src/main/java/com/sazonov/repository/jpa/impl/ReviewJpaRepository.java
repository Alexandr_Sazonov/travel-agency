package com.sazonov.repository.jpa.impl;

import com.sazonov.domain.entity.Review;
import com.sazonov.repository.CrudRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ReviewJpaRepository implements CrudRepository<Review> {

    @Autowired
    SessionFactory sessionFactory;

    CriteriaBuilder builder;
    CriteriaQuery<Review> criteria;
    Root<Review> root;
    Query<Review> query;


    @Override
    public List<Review> getAll() {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Review.class);
        root = criteria.from(Review.class);

        criteria.select(root);
        query = session.createQuery(criteria);

        return query.getResultList();
    }

    @Override
    public Review getEntityById(int id) {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(Review.class);
        root = criteria.from(Review.class);

        criteria.select(root);
        query = session.createQuery(criteria);

        Review review = query.setMaxResults(1).uniqueResult();
        session.close();

        return review;
    }

    @Override
    @Transactional
    public boolean removeById(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Review review = new Review();
        review.setId(id);

        session.delete(review);
        transaction.commit();
        session.close();


        return false;
    }

    @Override
    @Transactional
    public boolean add(Review review) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(review);
        transaction.commit();
        session.close();

        return false;
    }

    @Override
    @Transactional
    public boolean update(Review review) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        builder = session.getCriteriaBuilder();
        CriteriaUpdate<Review> criteriaUpdate = builder.createCriteriaUpdate(Review.class);
        root = criteriaUpdate.from(Review.class);

        criteriaUpdate.set("date", review.getDate());
        criteriaUpdate.set("text", review.getText());
        criteriaUpdate.set("user_id", review.getUserId());
        criteriaUpdate.set("tour_id", review.getTourId());
        criteriaUpdate.where(builder.equal(root.get("id"), review.getId()));

        int actualResult = session.createQuery(criteriaUpdate).executeUpdate();

        transaction.commit();
        session.close();

        return actualResult == 1;
    }
}
