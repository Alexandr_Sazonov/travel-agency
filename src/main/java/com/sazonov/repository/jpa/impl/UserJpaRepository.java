package com.sazonov.repository.jpa.impl;

import com.sazonov.domain.entity.User;
import com.sazonov.repository.CrudRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserJpaRepository implements CrudRepository<User> {

    private final static String SQL_DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
    private final static String SQL_UPDATE_USER_BY_ID = "UPDATE users  SET login = :newLogin , password = :newPassword where id = :uid ";

    @Autowired
    SessionFactory sessionFactory;

    CriteriaBuilder builder;
    CriteriaQuery<User> criteria;
    Root<User> root;
    Query<User> query;



    public List<User> getAll() {
        Session session = sessionFactory.openSession();
        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(User.class);
        root = criteria.from(User.class);


        criteria.select(root);


        query = session.createQuery(criteria);
        List<User> userList = query.getResultList();

        session.close();

        return userList;
    }

    public User getEntityById(int id) {

        Session session = sessionFactory.openSession();

        builder = session.getCriteriaBuilder();
        criteria = builder.createQuery(User.class);
        root = criteria.from(User.class);

        criteria.select(root).where(builder.equal(root.get("id"), id));

        query = session.createQuery(criteria);

        User user = query.getSingleResult();

        session.close();

        return user;
    }


    @Transactional
    public boolean removeById(int id) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        query = session.createSQLQuery(SQL_DELETE_USER_BY_ID);
        query.setParameter(1, id);

        int result = query.executeUpdate();

        transaction.commit();
        session.close();

        return result == 1;
    }

    @Transactional
    public boolean add(User user) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(user);
        transaction.commit();
        session.close();

        return false;
    }

    public boolean update(User user) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        query = session.createSQLQuery(SQL_UPDATE_USER_BY_ID);
        query.setParameter("newLogin", user.getLogin());
        query.setParameter("newPassword", user.getPassword());
        query.setParameter("uid", user.getId());

        int actualResult = query.executeUpdate();

        transaction.commit();
        session.close();

        return actualResult == 1;
    }
}
