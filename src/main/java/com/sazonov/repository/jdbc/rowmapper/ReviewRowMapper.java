package com.sazonov.repository.jdbc.rowmapper;

import com.sazonov.domain.entity.Review;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The type Review row mapper.
 */
public class ReviewRowMapper implements RowMapper<Review> {

    private final static String FIELD_ID = "id";
    private final static String FIELD_DATE = "date";
    private final static String FIELD_TEXT = "text";
    private final static String FIELD_USER_ID = "user_id";
    private final static String FIELD_TOUR_ID = "tour_id";


    public Review mapRow(ResultSet resultSet, int i) throws SQLException {
        Review review = new Review();


        review.setId(resultSet.getInt(FIELD_ID));
        review.setDate(resultSet.getDate(FIELD_DATE));
        review.setText(resultSet.getString(FIELD_TEXT));
        review.setUserId(resultSet.getInt(FIELD_USER_ID));
        review.setTourId(resultSet.getInt(FIELD_TOUR_ID));


        return review;

    }
}
