package com.sazonov.repository.jdbc.rowmapper;

import com.sazonov.domain.entity.Tour;
import com.sazonov.domain.entity.TourType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The type Tour row mapper.
 */
public class TourRowMapper implements RowMapper<Tour> {
    private final static String FIELD_ID = "id";
    private final static String FIELD_PHOTO_PATH = "photo";
    private final static String FIELD_DATE = "date";
    private final static String FIELD_DURATION = "duration";
    private final static String FIELD_DESCRIPTION = "description";
    private final static String FIELD_COST = "cost";
    private final static String FIELD_HOTEL_ID = "hotel_id";
    private final static String FIELD_COUNTRY_ID = "country_id";
    private final static String FIELD_TOUR_TYPE = "tour_type";


    public Tour mapRow(ResultSet resultSet, int i) throws SQLException {
        Tour tour = new Tour();

        tour.setId(resultSet.getInt(FIELD_ID));
        tour.setPhoto(resultSet.getString(FIELD_PHOTO_PATH));
        tour.setDate(resultSet.getDate(FIELD_DATE));
        tour.setDuration(resultSet.getInt(FIELD_DURATION));
        tour.setDescription(resultSet.getString(FIELD_DESCRIPTION));
        tour.setCost(resultSet.getBigDecimal(FIELD_COST));
        tour.setTourType(TourType.valueOf(resultSet.getString(FIELD_TOUR_TYPE).toUpperCase()));
        tour.setHotelId(resultSet.getInt(FIELD_HOTEL_ID));
        tour.setCountryId(resultSet.getInt(FIELD_COUNTRY_ID));

        return tour;
    }

}
