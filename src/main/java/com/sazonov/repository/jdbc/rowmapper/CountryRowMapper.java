package com.sazonov.repository.jdbc.rowmapper;

import com.sazonov.domain.entity.Country;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The type Country row mapper.
 *
 * @author alexander
 * @version 1.0
 */
public class CountryRowMapper implements RowMapper<Country> {
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";

    public Country mapRow(ResultSet resultSet, int i) throws SQLException {
        Country country = new Country();

        country.setId(resultSet.getInt(FIELD_ID));
        country.setName(resultSet.getString(FIELD_NAME));

        return country;
    }

}
