package com.sazonov.repository.jdbc.rowmapper;

import com.sazonov.domain.entity.Hotel;
import com.sazonov.repository.jdbc.dbutil.EnumHelper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Hotel row mapper.
 */
public class HotelRowMapper implements RowMapper<Hotel> {
    private final static String FIELD_ID = "id";
    private final static String FIELD_NAME = "name";
    private final static String FIELD_STARS = "stars";
    private final static String FIELD_WEBSITE = "website";
    private final static String FIELD_LALITUDE = "lalitude";
    private final static String FIELD_LONGITUDE = "longitude";
    private final static String FIELD_FEATURE = "features";



    public Hotel mapRow(ResultSet resultSet, int i) throws SQLException {
        Hotel hotel = new Hotel();

        hotel.setId(resultSet.getInt(FIELD_ID));
        hotel.setName(resultSet.getString(FIELD_NAME));
        hotel.setStars(resultSet.getInt(FIELD_STARS));
        hotel.setWebsite(resultSet.getString(FIELD_WEBSITE));
        hotel.setLalitude(resultSet.getString(FIELD_LALITUDE));
        hotel.setLongitude(resultSet.getString(FIELD_LONGITUDE));
        hotel.setFeatures(EnumHelper.getFeature(resultSet.getArray(FIELD_FEATURE)));

        return hotel;

    }
}
