package com.sazonov.repository.jdbc.rowmapper;

import com.sazonov.domain.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The type User row mapper.
 *
 * @author alexander
 * @version 1.0
 */
public class UserRowMapper implements RowMapper<User> {

    private static final String FIELD_ID = "id";
    private static final String FIELD_LOGIN = "login";
    private static final String FIELD_PASSWORD = "password";


    public User mapRow(ResultSet resultSet, int i) throws SQLException {

        User user = new User();


        user.setId(resultSet.getInt(FIELD_ID));
        user.setLogin(resultSet.getString(FIELD_LOGIN));
        user.setPassword(FIELD_PASSWORD);

        return user;
    }

}
