package com.sazonov.repository.jdbc.impl;

import com.sazonov.domain.entity.User;
import com.sazonov.repository.CrudRepository;
import com.sazonov.repository.jdbc.InitializeRepository;
import com.sazonov.repository.jdbc.rowmapper.UserRowMapper;
import org.springframework.stereotype.Repository;


import java.util.List;


/**
 * The type User repository.
 */
@Repository
public class UserJdbcRepository extends InitializeRepository implements CrudRepository<User> {

    private final static String SELECT_ALL_USERS = "SELECT * FROM users";
    private final static String SELECT_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
    private final static String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
    private final static String INSERT_USER = "INSERT INTO users(login, password) VALUES(?, ?)";
    private final static String UPDATE_USER = "UPDATE users SET login = ?, password = ? WHERE id = ?";



    public List<User> getAll() {
        return jdbcTemplate.query(SELECT_ALL_USERS, new UserRowMapper());
    }

    public User getEntityById(int id) {
        return jdbcTemplate.queryForObject(SELECT_USER_BY_ID, new Object[]{id}, new UserRowMapper());
    }

    public boolean removeById(int id) {
        return jdbcTemplate.update(DELETE_USER_BY_ID, id) == 1;
    }

    public boolean add(User user) {
        return jdbcTemplate.update(INSERT_USER, user.getLogin(), user.getPassword()) == 1;
    }

    public boolean update(User user) {
        return jdbcTemplate.update(UPDATE_USER, user.getLogin(), user.getPassword(), user.getId()) == 1;
    }
}
