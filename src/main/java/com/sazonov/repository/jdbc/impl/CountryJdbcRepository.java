package com.sazonov.repository.jdbc.impl;

import com.sazonov.domain.entity.Country;
import com.sazonov.repository.CrudRepository;
import com.sazonov.repository.jdbc.InitializeRepository;
import com.sazonov.repository.jdbc.rowmapper.CountryRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The type Country repository.
 */
@Repository
public class CountryJdbcRepository extends InitializeRepository implements CrudRepository<Country> {

    private static final String INSERT_COUNTRY = "INSERT INTO country (name) values (?)";
    private static final String SELECT_COUNTRY_BY_ID = "SELECT * FROM country WHERE id = ?";
    private static final String UPDATE_COUNTRY = "UPDATE country SET name = ? WHERE id = ?";
    private static final String DELETE_COUNTRY_BY_ID = "DELETE FROM country WHERE id = ?";
    private static final String SELECT_ALL_COUNTRY = "SELECT * FROM country";
    private static final String SELECT_COUNTRY_BY_NAME = "SELECT * FROM country WHERE name = ?";





    public List<Country> getAll() {
        return jdbcTemplate.query(SELECT_ALL_COUNTRY, new CountryRowMapper());
    }

    public Country getEntityById(int id) {
        return jdbcTemplate.queryForObject(SELECT_COUNTRY_BY_ID,new Object[]{id}, new CountryRowMapper());
    }

    public boolean removeById(int id) {
        return jdbcTemplate.update(DELETE_COUNTRY_BY_ID, id) == 1;
    }

    public boolean add(Country country) {
        return jdbcTemplate.update(INSERT_COUNTRY, country.getName()) == 1;
    }

    public boolean update(Country country) {
        return jdbcTemplate.update(UPDATE_COUNTRY, country.getName(), country.getId()) == 1;
    }
}
