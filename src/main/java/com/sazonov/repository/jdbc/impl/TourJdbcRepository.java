package com.sazonov.repository.jdbc.impl;

import com.sazonov.domain.entity.Tour;
import com.sazonov.repository.CrudRepository;
import com.sazonov.repository.jdbc.InitializeRepository;
import com.sazonov.repository.jdbc.rowmapper.TourRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The type Tour repositrory.
 */
@Repository
public class TourJdbcRepository extends InitializeRepository implements CrudRepository<Tour> {

    private final static String INSERT_TOUR = "INSERT INTO tour(photo, date, duration, description, cost, tour_type, hotel_id, country_id) VALUES(?, ?, ?, ?, ?, ?::tour_type, ?, ?)";
    private final static String UPDATE_TOUR = "UPDATE tour SET photo = ?, date = ?, duration = ?, description = ?, cost = ?, tour_type = ?::tour_type, hotel_id = ?, country_id = ? WHERE id = ?";
    private final static String DELETE_TOUR = "DELETE FROM tour WHERE id = ?";
    private final static String SELECT_TOUR_BY_ID = "SELECT * FROM tour WHERE id = ?";
    private final static String SELECT_ALL_TOUR = "SELECT * FROM tour";




    public List<Tour> getAll() {
        return jdbcTemplate.query(SELECT_ALL_TOUR, new TourRowMapper());
    }

    public Tour getEntityById(int id) {
        return jdbcTemplate.queryForObject(SELECT_TOUR_BY_ID, new Object[]{id}, new TourRowMapper());
    }

    public boolean removeById(int id) {
        return jdbcTemplate.update(DELETE_TOUR, id) == 1;
    }

    public boolean add(Tour tour) {
        return jdbcTemplate.update(INSERT_TOUR, tour.getPhoto(),
                tour.getDate(), tour.getDuration(), tour.getDescription(),
                tour.getCost(), tour.getTourType().toString().toLowerCase(),
                tour.getHotelId(), tour.getCountryId()
        ) == 1;
    }

    public boolean update(Tour tour) {
        return jdbcTemplate.update(UPDATE_TOUR, tour.getPhoto(),
                tour.getDate(), tour.getDuration(), tour.getDescription(),
                tour.getCost(), tour.getTourType().toString(),
                tour.getHotelId(), tour.getCountryId(), tour.getId()
        ) == 1;
    }
}
