package com.sazonov.repository.jdbc.impl;

import com.sazonov.domain.entity.Review;
import com.sazonov.repository.CrudRepository;
import com.sazonov.repository.jdbc.InitializeRepository;
import com.sazonov.repository.jdbc.rowmapper.ReviewRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Review repository.
 */
@Repository
public class ReviewJdbcRepository extends InitializeRepository implements CrudRepository<Review> {

    private final static String INSERT_REVIEW = "INSERT INTO review(date, text, user_id, tour_id) values(?, ?, ?, ?)";
    private final static String UPDATE_REVIEW = "UPDATE review SET date = ?, text = ?, user_id = ?, tour_id = ? WHERE id = ?";
    private final static String DELETE_REVIEW = "DELETE FROM review WHERE id = ?";
    private final static String SELECT_REVIEW_BY_ID = "SELECT * FROM review WHERE id = ?";
    private final static String SELECT_ALL_REVIEW = "SELECT * FROM review";




    public List<Review> getAll() {
        return jdbcTemplate.query(SELECT_ALL_REVIEW, new ReviewRowMapper());
    }

    public Review getEntityById(int id) {
        return jdbcTemplate.queryForObject(SELECT_REVIEW_BY_ID, new Object[]{id}, new ReviewRowMapper());
    }

    public boolean removeById(int id) {
        return jdbcTemplate.update(DELETE_REVIEW, id) == 1;
    }

    public boolean add(Review review) {
        return jdbcTemplate.update(INSERT_REVIEW, review.getDate(),
                review.getText(), review.getUserId(),
                review.getTourId()
        ) == 1;
    }

    public boolean update(Review review) {
        return jdbcTemplate.update(UPDATE_REVIEW, review.getDate(),
                review.getText(), review.getUserId(),
                review.getTourId(), review.getId()
        ) == 1;
    }
}
