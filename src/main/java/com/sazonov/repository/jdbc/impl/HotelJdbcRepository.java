package com.sazonov.repository.jdbc.impl;

import com.sazonov.domain.entity.Hotel;
import com.sazonov.repository.CrudRepository;
import com.sazonov.repository.jdbc.InitializeRepository;
import com.sazonov.repository.jdbc.dbutil.EnumHelper;
import com.sazonov.repository.jdbc.rowmapper.HotelRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * The type Hotel repository.
 */
@Repository
public class HotelJdbcRepository extends InitializeRepository implements CrudRepository<Hotel> {

    private final static String INSERT_HOTEL = "INSERT INTO hotel (name, stars, website, lalitude, longitude, features) VALUES (?, ?, ?, ?, ?, ARRAY [?]::features[])";
    private final static String UPDATE_HOTEL = "UPDATE hotel SET name = ?, stars = ?, website = ?, lalitude = ?, longitude = ?, features = ?::features[] WHERE id = ?";
    private final static String DELETE_HOTEL = "DELETE FROM hotel WHERE id = ?";
    private final static String SELECT_HOTEL_BY_ID = "SELECT * FROM hotel WHERE id = ?";
    private final static String SELECT_ALL_HOTEL = "SELECT * FROM hotel";
    private final static String SELECT_HOTEL_BY_NAME = "SELECT * FROM hotel WHERE name = ?";




    public List<Hotel> getAll() {
        return jdbcTemplate.query(SELECT_ALL_HOTEL, new HotelRowMapper());
    }

    public Hotel getEntityById(int id) {
        return jdbcTemplate.queryForObject(SELECT_HOTEL_BY_ID, new Object[]{id}, new HotelRowMapper());
    }

    public boolean removeById(int id) {
        return jdbcTemplate.update(DELETE_HOTEL, id) == 1;
    }

    public boolean add(Hotel hotel) {

        return jdbcTemplate.update(INSERT_HOTEL,
                hotel.getName(), hotel.getStars(),
                hotel.getWebsite(), hotel.getLalitude(),
                hotel.getLongitude(), EnumHelper.getArray(hotel.getFeatures())) == 1;
    }

    public boolean update(Hotel hotel) {
        return jdbcTemplate.update(UPDATE_HOTEL, hotel.getName(),
                hotel.getStars(), hotel.getWebsite(), hotel.getLalitude(),
                hotel.getLongitude(), EnumHelper.getArray(hotel.getFeatures()), hotel.getId()) == 1;
    }
}
