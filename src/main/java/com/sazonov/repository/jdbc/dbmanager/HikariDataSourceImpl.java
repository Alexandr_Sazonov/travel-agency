package com.sazonov.repository.jdbc.dbmanager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.SQLException;

/**
 * The type Hikari data source.
 */
public class HikariDataSourceImpl {

    private String propertiesPath;
    private static HikariConfig config;
    private static HikariDataSource dataSource;
    private static String PROPERTEIS_PATH = "src/main/resources/db.properties";


    public HikariDataSourceImpl(){
        propertiesPath = PROPERTEIS_PATH;

        config = new HikariConfig(propertiesPath);
        dataSource = new HikariDataSource(config);
    }

    public HikariDataSourceImpl(String propertiesPath) {
        propertiesPath = propertiesPath;
        config = new HikariConfig(propertiesPath);
        dataSource = new HikariDataSource(config);
    }

    public HikariDataSource getDataSource() {

        return dataSource;
    }
}
