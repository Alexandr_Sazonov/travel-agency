package com.sazonov.repository.jdbc.dbutil;

import com.sazonov.config.AppConfig;
import com.sazonov.domain.entity.Feature;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import javax.sql.DataSource;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Enum helper.
 */
public class EnumHelper {


    private static Array getArray(List<Feature> features){


        Feature[] featreArray = features.toArray(new Feature[features.size()]);
        String[] stringFeatureArray = new String[featreArray.length];

        for (int i=0;i<featreArray.length;i++) {
            stringFeatureArray[i] = featreArray[i].toString().toLowerCase();
        }

        try {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

            DataSource dataSource = context.getBean(DataSource.class);

            Connection connection = dataSource.getConnection();
            return connection.createArrayOf("features", stringFeatureArray);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static ArrayList<Feature> getFeatures(Array featureArray){
        ArrayList<Feature> features = new ArrayList<Feature>();

        try {
            String[] featureArrayString = (String[])featureArray.getArray();

            for (int i=0;i<featureArrayString.length;i++) {
                features.add(Feature.valueOf(featureArrayString[i].toUpperCase()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return features;
    }

    public static Feature[] getFeature(Array featureArray){
        ArrayList<Feature> featureList = getFeatures(featureArray);
        return featureList.toArray(new Feature[featureList.size()]);
    }

    public static Array getArray(Feature[] features){
        List<Feature> featureList = new ArrayList<>();

        for (int i=0;i<features.length;i++){
            featureList.add(features[i]);
        }

        return getArray(featureList);
    }
}
