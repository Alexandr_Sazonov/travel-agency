package com.sazonov.repository;

import java.util.List;

/**
 * The interface CrudRepository.
 *
 * @param <T> the type parameter
 * @author alexander
 * @version 1.0
 */
public interface CrudRepository<T> {

    /**
     * Gets all.
     *
     * @return the all
     */
    List<T> getAll();

    /**
     * Gets entity by id.
     *
     * @param id the id
     * @return the entity by id
     */
    T getEntityById(int id);

    /**
     * Remove by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    boolean removeById(int id);

    /**
     * Add boolean.
     *
     * @param t the t
     * @return the boolean
     */
    boolean add(T t);

    /**
     * Update boolean.
     *
     * @param t the t
     * @return the boolean
     */
    boolean update(T t);
}
