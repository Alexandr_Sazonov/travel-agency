package com.sazonov.config;

import com.sazonov.repository.jdbc.dbmanager.HikariDataSourceImpl;
import com.sazonov.repository.jpa.dbutil.HibernateUtil;
import com.sazonov.service.impl.UserServiceImpl;
import org.aspectj.lang.annotation.Before;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.sazonov")
public class AppConfig {


    @Bean
    public DataSource dataSource() {
        HikariDataSourceImpl hikariDataSource = new HikariDataSourceImpl();
        return hikariDataSource.getDataSource();
    }


    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    @Bean
    public SessionFactory sessionFactory(){
        return HibernateUtil.getSessionFactory();
    }

}
