package com.sazonov.service.impl;

import com.sazonov.domain.entity.Review;
import com.sazonov.repository.jdbc.impl.ReviewJdbcRepository;
import com.sazonov.repository.jpa.impl.ReviewJpaRepository;
import com.sazonov.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The type Review service.
 */
@Service
@Transactional
public class ReviewServiceImpl implements IService<Review> {

    @Autowired
    private ReviewJpaRepository reviewJpaRepository;

    public List<Review> getAll() {
        return reviewJpaRepository.getAll();
    }

    public Review getById(int id) {
        return reviewJpaRepository.getEntityById(id);
    }

    public boolean removeById(int id) {
        return reviewJpaRepository.removeById(id);
    }

    public boolean add(Review review) {
        return reviewJpaRepository.add(review);
    }

    public boolean update(Review review) {
        return reviewJpaRepository.update(review);
    }
}
