package com.sazonov.service.impl;

import com.sazonov.domain.entity.Tour;
import com.sazonov.repository.jdbc.impl.TourJdbcRepository;
import com.sazonov.repository.jpa.impl.TourJpaRepository;
import com.sazonov.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The type Tour service.
 */
@Service
@Transactional
public class TourServiceImpl implements IService<Tour> {

    @Autowired
    private TourJpaRepository tourJpaRepository;

    public List<Tour> getAll() {
        return tourJpaRepository.getAll();
    }

    public Tour getById(int id) {
        return tourJpaRepository.getEntityById(id);
    }

    public boolean removeById(int id) {
        return tourJpaRepository.removeById(id);
    }

    public boolean add(Tour tour) {
        return tourJpaRepository.add(tour);
    }

    public boolean update(Tour tour) {
        return tourJpaRepository.update(tour);
    }
}
