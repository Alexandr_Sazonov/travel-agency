package com.sazonov.service.impl;

import com.sazonov.domain.entity.Country;
import com.sazonov.repository.jdbc.impl.CountryJdbcRepository;
import com.sazonov.repository.jpa.impl.CountryJpaRepository;
import com.sazonov.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The type Country service.
 */
@Service
@Transactional
public class CountryServiceImpl implements IService<Country> {

    @Autowired
    CountryJpaRepository countryJpaRepository;

    public List<Country> getAll() {
        return countryJpaRepository.getAll();
    }

    public Country getById(int id) {
        return countryJpaRepository.getEntityById(id);
    }

    public boolean removeById(int id) {
        return countryJpaRepository.removeById(id);
    }

    public boolean add(Country country) {
        return countryJpaRepository.add(country);
    }

    public boolean update(Country country) {
        return countryJpaRepository.update(country);
    }
}
