package com.sazonov.service.impl;

import com.sazonov.domain.entity.Hotel;
import com.sazonov.repository.jdbc.impl.HotelJdbcRepository;
import com.sazonov.repository.jpa.impl.HotelJpaRepository;
import com.sazonov.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The type Hotel service.
 */
@Service
@Transactional
public class HotelServiceImpl implements IService<Hotel> {

    @Autowired
    private HotelJpaRepository hotelJpaRepository;

    public List<Hotel> getAll() {
        return hotelJpaRepository.getAll();
    }

    public Hotel getById(int id) {
        return hotelJpaRepository.getEntityById(id);
    }

    public boolean removeById(int id) {
        return hotelJpaRepository.removeById(id);
    }

    public boolean add(Hotel hotel) {
        return hotelJpaRepository.add(hotel);
    }

    public boolean update(Hotel hotel) {
        return hotelJpaRepository.update(hotel);
    }
}
