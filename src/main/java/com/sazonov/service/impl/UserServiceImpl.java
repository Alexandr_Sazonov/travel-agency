package com.sazonov.service.impl;

import com.sazonov.domain.entity.User;
import com.sazonov.repository.jdbc.impl.UserJdbcRepository;
import com.sazonov.repository.jpa.impl.UserJpaRepository;
import com.sazonov.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The type User service.
 */
@Service
@Transactional
public class UserServiceImpl implements IService<User> {

    @Autowired
    private UserJpaRepository userJpaRepository;

    public List<User> getAll() {
        return userJpaRepository.getAll();
    }

    public User getById(int id) {
        return userJpaRepository.getEntityById(id);
    }

    public boolean removeById(int id) {
        return userJpaRepository.removeById(id);
    }

    public boolean add(User user) {
        return userJpaRepository.add(user);
    }

    public boolean update(User user) {
        return userJpaRepository.update(user);
    }
}
