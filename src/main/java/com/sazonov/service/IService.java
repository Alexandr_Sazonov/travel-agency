package com.sazonov.service;

import java.util.List;


/**
 * The interface Service.
 *
 * @param <T> the type parameter
 */
public interface IService<T> {

    /**
     * Gets all.
     *
     * @return the all
     */
    List<T> getAll();

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    T getById(int id);

    /**
     * Remove by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    boolean removeById(int id);

    /**
     * Add boolean.
     *
     * @param t the t
     * @return the boolean
     */
    boolean add(T t);

    /**
     * Update boolean.
     *
     * @param t the t
     * @return the boolean
     */
    boolean update(T t);
}
