package com.sazonov.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

    @Before("execution(* com.sazonov.service.impl.*.update(..))")
    public void update() {
        LOGGER.info("Entity is updating");
    }

    @After("execution(* com.sazonov.service.impl.*.getAll())")
    public void getAll(JoinPoint joinPoint) {
        LOGGER.info("Entities were found - " + joinPoint.getSignature().getName());
    }

    @AfterReturning(
            pointcut = "execution(* com.sazonov.service.impl.*.add(..))",
            returning = "result")
    public void add(boolean result) {
        if (result) {
            LOGGER.info("Entity was added");
        } else {
            LOGGER.info("Entity wasn't added");
        }
    }

}
