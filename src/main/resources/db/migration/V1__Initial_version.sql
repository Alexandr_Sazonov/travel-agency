create type features as enum ('music', 'tv', 'wifi', 'phone', 'massage', 'computer', 'bar', 'bitch', 'cola', 'sprite', 'none');

create type tour_type as enum ('family', 'one', 'two', 'hot', 'bar', 'foo', 'tor', 'top', 'acc');


CREATE TABLE "hotel"
(
  id SERIAL PRIMARY KEY,
  name text,
  stars integer NOT NULL,
  website text,
  lalitude text NOT NULL,
  longitude text NOT NULL,
  features features[]
);

CREATE TABLE country (
                         id SERIAL PRIMARY KEY,
                         name varchar(100) default NULL
);

CREATE TABLE review
(
  id      SERIAL PRIMARY KEY,
  date    date    NOT NULL,
  text    text,
  user_id integer NOT NULL,
  tour_id integer NOT NULL
);

CREATE TABLE tour
(
  id SERIAL PRIMARY KEY,
  photo text,
  date date,
  duration integer NOT NULL,
  description text ,
  cost numeric NOT NULL,
  hotel_id integer NOT NULL,
  country_id integer NOT NULL,
  tour_type tour_type DEFAULT 'acc'::tour_type
);


CREATE TABLE users
(
  id SERIAL PRIMARY KEY,
  login character varying(255) ,
  password character varying(255)
);

CREATE TABLE usertour
(
  user_id integer NOT NULL,
  tour_id integer NOT NULL
);

