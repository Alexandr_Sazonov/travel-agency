package com.sazonov.repository.jdbc;

import com.sazonov.domain.entity.Country;
import com.sazonov.repository.jdbc.impl.CountryJdbcRepository;
import io.zonky.test.db.postgres.embedded.FlywayPreparer;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CountryJdbcRepositoryTest {
    static CountryJdbcRepository countryJdbcRepository;

    Country country = new Country(1, "test");

    @ClassRule
    public static PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @BeforeClass
    public static void setUp() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db.getTestDatabase());

        countryJdbcRepository = new CountryJdbcRepository();
        countryJdbcRepository.jdbcTemplate = jdbcTemplate;

    }


    @Test
    public void getEntityByIdTest(){
        Country country = countryJdbcRepository.getEntityById(1);

        boolean expected = true;
        boolean actual = country != null;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getAllTest(){
        List<Country> countryList = countryJdbcRepository.getAll();

        boolean expected = true;
        boolean actual = countryList.size() > 0;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeById(){

        boolean expected = true;
        boolean actual = countryJdbcRepository.removeById(1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addTest(){

        boolean expected = true;
        boolean actual = countryJdbcRepository.add(country);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){

        boolean expected = false;
        boolean actual = countryJdbcRepository.update(new Country());

        Assert.assertEquals(expected, actual);
    }
}
