package com.sazonov.repository.jdbc;

import com.sazonov.domain.entity.User;
import com.sazonov.repository.jdbc.impl.UserJdbcRepository;
import io.zonky.test.db.postgres.embedded.FlywayPreparer;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class UserJdbcRepositoryTest {
    static UserJdbcRepository userJdbcRepository = new UserJdbcRepository();

    @ClassRule
    public static PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @BeforeClass
    public static void setUp() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db.getTestDatabase());

        userJdbcRepository = new UserJdbcRepository();
        userJdbcRepository.jdbcTemplate = jdbcTemplate;

    }

    @Test
    public void getEntityByIdTest(){
        User user = userJdbcRepository.getEntityById(1);

        boolean expected = true;
        boolean actual = user != null;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getAllTest(){
        List<User> users = userJdbcRepository.getAll();

        boolean expected = true;
        boolean actual = users.size() > 0;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeById(){

        boolean expected = true;
        boolean actual = userJdbcRepository.removeById(1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addTest(){

        boolean expected = true;
        boolean actual = userJdbcRepository.add(new User(1, "ss", "ss"));

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){

        boolean expected = false;
        boolean actual = userJdbcRepository.update(new User());

        Assert.assertEquals(expected, actual);
    }
}
