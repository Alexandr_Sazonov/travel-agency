package com.sazonov.repository.jdbc;

import com.sazonov.domain.entity.Feature;
import com.sazonov.domain.entity.Hotel;
import com.sazonov.repository.jdbc.impl.HotelJdbcRepository;
import io.zonky.test.db.postgres.embedded.FlywayPreparer;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.PreparedDbRule;
import org.junit.*;
import org.springframework.jdbc.core.JdbcTemplate;


public class HotelJdbcRepositoryTest {
    static HotelJdbcRepository hotelJdbcRepository = new HotelJdbcRepository();


    Hotel hotel;


    @ClassRule
    public static PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @BeforeClass
    public static void setUp() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db.getTestDatabase());

        hotelJdbcRepository = new HotelJdbcRepository();
        hotelJdbcRepository.jdbcTemplate = jdbcTemplate;


    }


    @Before
    public void initHotel(){
        hotel = new Hotel();
        hotel.setId(1);
        hotel.setLalitude("123");
        hotel.setLongitude("312");
        hotel.setName("name");
        hotel.setStars(5);
        hotel.setWebsite("vl.kas");
        Feature[] features = new Feature[]{Feature.tv, Feature.cola};
        hotel.setFeatures(features);
    }

    /*@Test
    public void addTest(){

        boolean expected = true;
        boolean actual = hotelRepository.add(hotel);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getEntityByIdTest(){

        boolean expected = true;
        boolean actual = hotelRepository.getEntityById(1) != null;

        Assert.assertEquals(expected, actual);
    }*/

    @Test
    public void getAllTest(){

        boolean expected = true;
        boolean actual = hotelJdbcRepository.getAll().size() > 1;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeByIdTest(){

        boolean expected = true;
        boolean actual = hotelJdbcRepository.removeById(1);

        Assert.assertEquals(expected, actual);
    }
}
