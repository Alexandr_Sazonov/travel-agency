package com.sazonov.repository.jdbc;

import com.sazonov.domain.entity.Review;
import com.sazonov.repository.jdbc.impl.ReviewJdbcRepository;
import io.zonky.test.db.postgres.embedded.FlywayPreparer;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Date;

public class ReviewJdbcRepositoryTest {
    static ReviewJdbcRepository reviewJdbcRepository = new ReviewJdbcRepository();

    Review review = new Review(1, new Date(), "sometext", 1, 1);

    @ClassRule
    public static PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @BeforeClass
    public static void setUp() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db.getTestDatabase());
        reviewJdbcRepository = new ReviewJdbcRepository();
        reviewJdbcRepository.jdbcTemplate = jdbcTemplate;


    }

    @Test
    public void getAllTest(){

        boolean expected = true;
        boolean actual = reviewJdbcRepository.getAll().size() > 1;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getEntityById(){

        boolean expected = true;
        boolean actual = reviewJdbcRepository.getEntityById(1) != null;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeByIdTest(){

        boolean expected = true;
        boolean actual = reviewJdbcRepository.removeById(1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addTest(){

        boolean expected = true;
        boolean actual = reviewJdbcRepository.add(review);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){

        boolean expected = false;
        boolean actual = reviewJdbcRepository.update(new Review());

        Assert.assertEquals(expected, actual);
    }
}
