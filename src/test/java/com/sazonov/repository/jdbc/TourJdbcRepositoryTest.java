package com.sazonov.repository.jdbc;

import com.sazonov.domain.entity.Tour;
import com.sazonov.domain.entity.TourType;
import com.sazonov.repository.jdbc.impl.TourJdbcRepository;
import io.zonky.test.db.postgres.embedded.FlywayPreparer;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.math.BigDecimal;
import java.util.Date;

public class TourJdbcRepositoryTest {
    static TourJdbcRepository tourJdbcRepository = new TourJdbcRepository();

    static Tour tour;

    @ClassRule
    public static PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @BeforeClass
    public static void setUp() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db.getTestDatabase());

        tourJdbcRepository = new TourJdbcRepository();
        tourJdbcRepository.jdbcTemplate = jdbcTemplate;

        tour = new Tour(1, "photo", new Date(), 1, "desc", new BigDecimal(1), TourType.family,1, 1);

    }

    @Test
    public void getAllTest(){

        boolean expected = true;
        boolean actual = tourJdbcRepository.getAll().size() > 1;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getEntityById(){

        boolean expected = true;
        boolean actual = tourJdbcRepository.getEntityById(1) != null;

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeById(){

        boolean expected = true;
        boolean actual = tourJdbcRepository.removeById(1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addTest(){

        boolean expected = true;
        boolean actual = tourJdbcRepository.add(tour);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){

        boolean expected = false;
        boolean actual = true;
        try {
            tourJdbcRepository.update(new Tour());
        } catch (NullPointerException e){
            actual = false;
        }


        Assert.assertEquals(expected, actual);
    }
}
