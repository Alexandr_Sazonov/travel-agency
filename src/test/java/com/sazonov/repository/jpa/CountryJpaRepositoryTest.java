package com.sazonov.repository.jpa;

import com.sazonov.config.AppConfig;
import com.sazonov.domain.entity.Country;
import com.sazonov.repository.jpa.impl.CountryJpaRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class CountryJpaRepositoryTest {

    @Autowired
    CountryJpaRepository countryJpaRepository;

    Country country = new Country(1, "test");

    @Test
    public void getAllTest(){
        boolean result = countryJpaRepository.getAll().size() > 0;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void getEntityById(){
        boolean result = countryJpaRepository.getEntityById(1) != null;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void addTest(){
        boolean expected = true;
        boolean actual = countryJpaRepository.add(country);

        List<Country> countries = countryJpaRepository.getAll();

        for (Country c: countries) {
            if (country.getName().equals(c.getName())) {
                actual = true;
            }
        }

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){
        boolean expected = true;

        Country country1 = countryJpaRepository.getEntityById(1);
        country1.setName("qq");

        boolean actual = countryJpaRepository.update(country1);

        Assert.assertEquals(expected, actual);
    }
}
