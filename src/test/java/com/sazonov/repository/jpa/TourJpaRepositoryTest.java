package com.sazonov.repository.jpa;

import com.sazonov.config.AppConfig;
import com.sazonov.domain.entity.Hotel;
import com.sazonov.domain.entity.Tour;
import com.sazonov.domain.entity.TourType;
import com.sazonov.repository.jdbc.impl.TourJdbcRepository;
import com.sazonov.repository.jpa.impl.HotelJpaRepository;
import com.sazonov.repository.jpa.impl.TourJpaRepository;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class TourJpaRepositoryTest {

    static Tour tour;

    @Autowired
    TourJpaRepository repository;


    @BeforeClass
    public static void setUp() {
        tour = new Tour(1, "photo", new Date(), 1, "desc", new BigDecimal(1), TourType.family,1, 1);
    }

    @Test
    public void getAllTest(){
        boolean result = repository.getAll().size() > 0;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void getEntityById(){
        boolean result = repository.getEntityById(1) != null;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void addTest(){
        boolean expected = true;
        boolean actual = repository.add(tour);

        List<Tour> tours = repository.getAll();

        for (Tour c: tours) {
            if (tour.getDescription().equals(c.getDescription())) {
                actual = true;
            }
        }

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){
        boolean expected = true;

        Tour tour1 = repository.getEntityById(1);
        tour1.setDescription("qq");

        boolean actual = repository.update(tour1);

        Assert.assertEquals(expected, actual);
    }
}
