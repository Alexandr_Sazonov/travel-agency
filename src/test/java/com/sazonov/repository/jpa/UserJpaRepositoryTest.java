package com.sazonov.repository.jpa;


import com.sazonov.config.AppConfig;
import com.sazonov.domain.entity.Tour;
import com.sazonov.domain.entity.User;
import com.sazonov.repository.jpa.impl.TourJpaRepository;
import com.sazonov.repository.jpa.impl.UserJpaRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class UserJpaRepositoryTest {

    @Autowired
    UserJpaRepository repository;

    User user = new User(1, "test", "test");

    @Test
    public void getAllTest(){
        boolean result = repository.getAll().size() > 0;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void getEntityById(){
        boolean result = repository.getEntityById(1) != null;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }


    @Test
    public void updateTest(){
        boolean expected = true;

        User user1 = repository.getEntityById(1);
        user1.setLogin("qq");

        boolean actual = repository.update(user1);

        Assert.assertEquals(expected, actual);
    }
}
