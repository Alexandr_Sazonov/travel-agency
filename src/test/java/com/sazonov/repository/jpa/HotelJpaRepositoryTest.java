package com.sazonov.repository.jpa;


import com.sazonov.config.AppConfig;
import com.sazonov.domain.entity.Country;
import com.sazonov.domain.entity.Feature;
import com.sazonov.domain.entity.Hotel;
import com.sazonov.repository.jpa.impl.HotelJpaRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class HotelJpaRepositoryTest {

    @Autowired
    HotelJpaRepository repository;

    Hotel hotel;

    @Before
    public void initHotel(){
        hotel = new Hotel();
        hotel.setId(1);
        hotel.setLalitude("123");
        hotel.setLongitude("312");
        hotel.setName("name");
        hotel.setStars(5);
        hotel.setWebsite("vl.kas");
        Feature[] features = new Feature[]{Feature.tv, Feature.cola};
        hotel.setFeatures(features);
    }


    @Test
    public void getAllTest(){
        boolean result = repository.getAll().size() > 0;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void getEntityById(){
        boolean result = repository.getEntityById(1) != null;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void addTest(){
        boolean expected = true;
        boolean actual = repository.add(hotel);

        List<Hotel> hotels = repository.getAll();

        for (Hotel c: hotels) {
            if (hotel.getName().equals(c.getName())) {
                actual = true;
            }
        }

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void updateTest(){
        boolean expected = true;

        Hotel hotel1 = repository.getEntityById(1);
        hotel1.setName("qq");

        boolean actual = repository.update(hotel1);

        Assert.assertEquals(expected, actual);
    }
}
