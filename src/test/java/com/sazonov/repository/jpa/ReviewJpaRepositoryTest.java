package com.sazonov.repository.jpa;

import com.sazonov.config.AppConfig;
import com.sazonov.domain.entity.Hotel;
import com.sazonov.domain.entity.Review;
import com.sazonov.repository.jpa.impl.HotelJpaRepository;
import com.sazonov.repository.jpa.impl.ReviewJpaRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class ReviewJpaRepositoryTest {


    @Autowired
    ReviewJpaRepository repository;

    Review review = new Review(1, new Date(), "sometext", 1, 1);


    @Test
    public void getAllTest(){
        boolean result = repository.getAll().size() > 0;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void getEntityById(){
        boolean result = repository.getEntityById(1) != null;
        boolean expect = true;

        Assert.assertEquals(result, expect);
    }

    @Test
    public void addTest(){
        boolean expected = true;
        boolean actual = repository.add(review);

        List<Review> reviews = repository.getAll();

        for (Review c: reviews) {
            if (review.getText().equals(c.getText())) {
                actual = true;
            }
        }

        Assert.assertEquals(expected, actual);
    }


}
