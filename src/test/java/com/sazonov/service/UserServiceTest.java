package com.sazonov.service;

import com.sazonov.domain.entity.User;
import com.sazonov.service.impl.UserServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    UserServiceImpl userService;

    User user;

    @Before
    public void init(){
        userService = mock(UserServiceImpl.class);

        user = new User(1, "test", "test");
    }

    @After
    public void remove(){
        user = null;
    }


    @Test
    public void getEntityByIdTest(){
        when(userService.getById(user.getId())).thenReturn(user);
        userService.getById(user.getId());
        verify(userService).getById(user.getId());
    }

    @Test
    public void getAllTest(){
        userService.getAll();
        verify(userService).getAll();
    }

    @Test
    public void removeByIdTest(){
        when(userService.removeById(user.getId())).thenReturn(true);
        userService.removeById(user.getId());
        verify(userService).removeById(user.getId());
    }

    @Test
    public void updateTest(){
        when(userService.update(user)).thenReturn(true);
        userService.update(user);
        verify(userService).update(user);
    }


    @Test
    public void addTest(){
        when(userService.add(user)).thenReturn(true);
        userService.add(user);
        verify(userService).add(user);
    }
}
