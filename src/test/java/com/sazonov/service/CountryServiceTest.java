package com.sazonov.service;

import com.sazonov.domain.entity.Country;
import com.sazonov.service.impl.CountryServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {

    CountryServiceImpl countryService;

    Country country;

    @Before
    public void init(){
        countryService = mock(CountryServiceImpl.class);
        country = new Country();
        country.setId(1);
        country.setName("Kazakhstan");
    }

    @After
    public void clean(){
        country = null;
    }

    @Test
    public void getAllTest(){
        countryService.getAll();
        verify(countryService).getAll();
    }

    @Test
    public void removeByIdTest(){
        when(countryService.removeById(country.getId())).thenReturn(true);
        countryService.removeById(country.getId());
        verify(countryService).removeById(country.getId());
    }

    @Test
    public void updateTest(){
        when(countryService.update(country)).thenReturn(true);
        countryService.update(country);
        verify(countryService).update(country);
    }


    @Test
    public void addTest(){
        when(countryService.add(country)).thenReturn(true);
        countryService.add(country);
        verify(countryService).add(country);
    }

}
