package com.sazonov.service;

import com.sazonov.domain.entity.Feature;
import com.sazonov.domain.entity.Hotel;
import com.sazonov.service.impl.HotelServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HotelServiceTest {

    HotelServiceImpl hotelService;

    Hotel hotel;

    @Before
    public void init(){
        hotelService = mock(HotelServiceImpl.class);
        hotel = new Hotel();
        hotel.setId(1);
        hotel.setLalitude("123");
        hotel.setLongitude("312");
        hotel.setName("name");
        hotel.setStars(5);
        hotel.setWebsite("vl.kas");
        Feature[] features = new Feature[]{Feature.tv, Feature.cola};
        hotel.setFeatures(features);
    }

    @After
    public void clean(){
        hotel = null;
    }

    @Test
    public void getEntityByIdTest(){
        when(hotelService.getById(hotel.getId())).thenReturn(hotel);
        hotelService.getById(hotel.getId());
        verify(hotelService).getById(hotel.getId());
    }

    @Test
    public void getAllTest(){
        hotelService.getAll();
        verify(hotelService).getAll();
    }

    @Test
    public void removeByIdTest(){
        when(hotelService.removeById(hotel.getId())).thenReturn(true);
        hotelService.removeById(hotel.getId());
        verify(hotelService).removeById(hotel.getId());
    }

    @Test
    public void updateTest(){
        when(hotelService.update(hotel)).thenReturn(true);
        hotelService.update(hotel);
        verify(hotelService).update(hotel);
    }


    @Test
    public void addTest(){
        when(hotelService.add(hotel)).thenReturn(true);
        hotelService.add(hotel);
        verify(hotelService).add(hotel);
    }
}
