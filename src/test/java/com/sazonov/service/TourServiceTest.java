package com.sazonov.service;

import com.sazonov.domain.entity.Tour;
import com.sazonov.domain.entity.TourType;
import com.sazonov.service.impl.TourServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TourServiceTest {

    TourServiceImpl tourService;

    Tour tour;

    @Before
    public void init(){
        tourService = mock(TourServiceImpl.class);
        tour = new Tour(1, "photo", new Date(), 1, "desc", new BigDecimal(1), TourType.family,1, 1);
    }

    @After
    public void clean(){
        tour = null;
    }

    @Test
    public void getAllTest(){
        tourService.getAll();
        verify(tourService).getAll();
    }

    @Test
    public void removeByIdTest(){
        when(tourService.removeById(tour.getId())).thenReturn(true);
        tourService.removeById(tour.getId());
        verify(tourService).removeById(tour.getId());
    }

    @Test
    public void updateTest(){
        when(tourService.update(tour)).thenReturn(true);
        tourService.update(tour);
        verify(tourService).update(tour);
    }


    @Test
    public void addTest(){
        when(tourService.add(tour)).thenReturn(true);
        tourService.add(tour);
        verify(tourService).add(tour);
    }

    @Test
    public void getEntityByIdTest(){
        when(tourService.getById(tour.getId())).thenReturn(tour);
        tourService.getById(tour.getId());
        verify(tourService).getById(tour.getId());
    }
}
