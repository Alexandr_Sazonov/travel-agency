package com.sazonov.service;

import com.sazonov.domain.entity.Review;
import com.sazonov.service.impl.ReviewServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ReviewServiceTest {

    ReviewServiceImpl reviewService;

    Review review;

    @Before
    public void init(){
        reviewService = mock(ReviewServiceImpl.class);
        review = new Review(1, new Date(), "sometext", 1, 1);
    }

    @After
    public void clean(){
        review = null;
    }

    @Test
    public void getAllTest(){
        reviewService.getAll();
        verify(reviewService).getAll();
    }

    @Test
    public void removeByIdTest(){
        when(reviewService.removeById(review.getId())).thenReturn(true);
        reviewService.removeById(review.getId());
        verify(reviewService).removeById(review.getId());
    }

    @Test
    public void updateTest(){
        when(reviewService.update(review)).thenReturn(true);
        reviewService.update(review);
        verify(reviewService).update(review);
    }


    @Test
    public void addTest(){
        when(reviewService.add(review)).thenReturn(true);
        reviewService.add(review);
        verify(reviewService).add(review);
    }

    @Test
    public void getEntityByIdTest(){
        when(reviewService.getById(review.getId())).thenReturn(review);
        reviewService.getById(review.getId());
        verify(reviewService).getById(review.getId());
    }
}
